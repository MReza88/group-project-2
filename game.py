import random
import os.path

def start():
    word = random.choice(word_list)

    while True:
        print(word)
        typed_word = input("Type in a word: ")
        if typed_word == word:
            score += 1
            print("Correct!")
            break
    else:
        print("Incorrect. The word was", word, "\n")
    print("You scored", score, "points.")


def file_reader():
    """reading the file"""
    lines = []
    with open("random_words.csv", 'r') as file:
        for rawline in file:
            lines.append(rawline.strip("\n"))
    return lines


if __name__ == '__main__':
    game = Game()
    game.start()
