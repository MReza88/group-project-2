DROP SCHEMA public CASCADE;
CREATE SCHEMA public;


CREATE TABLE player (
    id SERIAL PRIMARY KEY not null,
    name TEXT not null
);

CREATE TABLE game (
    id SERIAL PRIMARY KEY not null,
    randomword TEXT not null,
    start_time TIMESTAMP without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    player_id INT REFERENCES player(id) not null,
    result FLOAT
);

CREATE TABLE leaderboard (
    id SERIAL PRIMARY KEY not null,
    game_id INT REFERENCES game(id) NOT NULL,
    player_id INT REFERENCES player(id) NOT NULL 
);

CREATE TABLE typedwords (
    id SERIAL PRIMARY KEY NOT NULL,
    typedword TEXT NOT NULL,
    end_time TIMESTAMP without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL, 
    speed FLOAT,
    game_id INT REFERENCES game(id) NOT NULL
);

