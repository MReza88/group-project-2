from flask import Flask, session, render_template, request, redirect, url_for
import random
import postgresqlite
import datetime
import time
import os

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True
db = postgresqlite.connect()
app.config.update(SECRET_KEY=os.urandom(16), ENV='development')

# make sure all words are separated by [TAB]
with open('random_words.csv') as file:
    words = file.read().strip().split("\n")

# def get_random_word():
#     return db.query_value("SELECT randomword FROM game WHERE id = :game_id", game_id=session['game_id'])

@app.route('/', methods = ['GET', 'POST'])
def lobbyroom():
    """The main page, when the user(s) arrives at the website
    serving as a 'lobby' room for the 2 players.
    -- Perhaps in the beginning it should be only for 1 player"""

    if request.method == 'POST':
        # game init
        session['user_id'] = db.query_value("INSERT INTO player(name) VALUES('player111') returning id")
        session['in_progress'] = False
        return redirect(url_for('gameplay'))

    if 'message' in session:
        return render_template("lobby.html", message =  session['message'])
    else:
        return render_template("lobby.html", message = '')

@app.route('/playgame', methods = ['GET', 'POST'])
def gameplay():
    """The gameplay page"""
    if request.method == 'GET':
        if not session['in_progress']:
            word = random.choice(words)
            session['game_id'] = db.query_value("INSERT INTO game(randomword, player_id) VALUES(:word, :id) RETURNING id", word=word, id =  session['user_id'])
            word = db.query_value("SELECT randomword FROM game WHERE id = :game_id", game_id=session['game_id'])
            session['in_progress'] = True
            return render_template("playgame.html", message=word )
            
        print("Were in the GET PLAYGAME")
        start_time = db.query_value("SELECT start_time FROM game WHERE id = :game_id", game_id=session['game_id'])
       
    elif request.method == 'POST':
        print("Were in the POST PLAYGAME")
        playerguess = request.form["playerguess"]
        # player_guess_time = request.form["playertime"]
        db.query("INSERT INTO typedwords(typedword, game_id) VALUES(:playerguess, :id)", playerguess=playerguess, id=session['game_id'])

        speed = db.query_value("SELECT (game.start_time - typedwords.end_time) AS speed FROM game JOIN typedwords ON game.id = typedwords.game_id WHERE game.id = :game_id", game_id=session['game_id'])
        speed = str(speed)[5:]
        speed = round(float(speed), 2) - .11
        db.query("UPDATE typedwords SET speed = :speed WHERE game_id = :id", id=session['game_id'], speed=speed)

        if playerguess == db.query_value("SELECT randomword FROM game WHERE id = :game_id", game_id=session['game_id']):
            session['message']=f"Nailed it, bruh, your speed: {speed} seconds."
        else:
            session['message']="Nope"
        return redirect(url_for('lobbyroom'))

        session['in_progress'] = False
        return render_template("playgame.html", message=message )

    session['message']='You lose'
    return redirect(url_for('lobbyroom'))

# please make shore to follow this steps before you start the program
# -- To (re)create a database with this schema, use the following shell command:
# -- poetry run postgresqlite < example-schema.sql 

# poetry run postgresqlite pgcli
# DROP SCHEMA public;
# CREATE SCHEMA public;
# poetry run flask run --reload